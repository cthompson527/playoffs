FROM rust:latest AS dependencies
RUN cargo install diesel_cli
COPY Cargo.toml ./Cargo.toml
COPY Cargo.lock ./Cargo.lock
RUN mkdir src && echo "fn main() {}" > src/main.rs && cargo build && cargo build --release

FROM dependencies AS build-develop
COPY . .
RUN cargo build

FROM dependencies AS build-release
COPY . .
RUN cargo build --release

FROM alpine AS release
COPY --from=build-release ./target/release/playoffs_rs ./playoffs_rs

CMD ["./playoffs_rs"]
