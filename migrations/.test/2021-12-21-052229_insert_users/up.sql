-- Your SQL goes here
INSERT INTO users (username, password) VALUES ('Cory', '$2b$12$FblqKjsspFT.k2XB7uzvZ.C2Iu6dpBLFnGcMJU1Ca7wAygF09pPB6');
INSERT INTO users (username, password) VALUES ('Billy Bob', '$2b$12$FblqKjsspFT.k2XB7uzvZ.C2Iu6dpBLFnGcMJU1Ca7wAygF09pPB6');
INSERT INTO users (username, password) VALUES ('Frank', '$2b$12$FblqKjsspFT.k2XB7uzvZ.C2Iu6dpBLFnGcMJU1Ca7wAygF09pPB6');
