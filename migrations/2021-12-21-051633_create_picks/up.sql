-- Your SQL goes here
CREATE TABLE picks (
  id SERIAL PRIMARY KEY,
  user_id INT NOT NULL REFERENCES users(id),
  team_id INT NOT NULL REFERENCES teams(id),
  confidence INT NOT NULL
);