use std::fs;
use std::env;
use jwt_simple::prelude::*;
use rocket::{request::{FromRequest, Outcome}, Request, http::Status};
use serde::{Serialize, Deserialize};

lazy_static! {
    static ref PRIVATE_PEM_FILE_CONTENT: String = fs::read_to_string(env::var("PRIVATE_PEM").unwrap()).expect("could not read private key");
    static ref PUBLIC_PEM_FILE_CONTENT: String = fs::read_to_string(env::var("PUBLIC_PEM").unwrap()).expect("could not read public key");
    pub static ref PRIVATE_KEY: RS384KeyPair = RS384KeyPair::from_pem(PRIVATE_PEM_FILE_CONTENT.as_str()).expect("could not create private key object");
    pub static ref PUBLIC_KEY: RS384PublicKey = RS384PublicKey::from_pem(PUBLIC_PEM_FILE_CONTENT.as_str()).expect("could not create public key object");
}

#[derive(Serialize, Deserialize, Debug)]
pub struct TokenBody {
    pub id: i32,
    pub username: String,
}

#[derive(Debug)]
pub enum ApiKeyError {
    Missing,
    Invalid,
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for TokenBody {
    type Error = ApiKeyError;

    async fn from_request(req: &'r Request<'_>) -> Outcome<Self, Self::Error> {
        fn is_valid(key: &str) -> bool {
            match get_user(key) {
                None => false,
                Some(_) => true,
            }
        }

        fn get_user(key: &str) -> Option<TokenBody> {
            if let Some(key) = key.split("Bearer ").skip(1).next() {
                verify_token(key)
            } else {
                None
            }
        }
        
        match req.headers().get_one("Authorization") {
            None => Outcome::Failure((Status::BadRequest, ApiKeyError::Missing)),
            Some(key) if is_valid(key) => Outcome::Success(get_user(key).expect("hit an error. should only have a valid key here")),
            Some(_) => Outcome::Failure((Status::BadRequest, ApiKeyError::Invalid)),
        }
    }
}


pub fn create_token(token: TokenBody) -> String {
    let token = Claims::with_custom_claims(token, Duration::from_days(365));
    match (*PRIVATE_KEY).sign(token) {
        Ok(it) => it,
        Err(_) => "".to_string(),
    }
}

fn verify_token(token: &str) -> Option<TokenBody> {
    match (*PUBLIC_KEY).verify_token::<TokenBody>(&token, None) {
        Ok(token) => Some(token.custom),
        Err(_) => None,
    }
}