use rocket_sync_db_pools::{database, diesel as DieselPgPool};

#[database("pg_db")]
pub struct PgDb(DieselPgPool::PgConnection);
