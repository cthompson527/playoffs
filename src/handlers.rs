use itertools::Itertools;
use diesel::prelude::*;
use rocket::serde::json::Json;
use serde::{Deserialize, Serialize};
use bcrypt::{DEFAULT_COST, hash, verify};
use crate::connection::PgDb;
use crate::schema;
use crate::models;
use crate::auth;
use crate::time_guards;

#[derive(Deserialize)]
pub struct User<'r> {
    username: &'r str,
    password: &'r str,
}

#[derive(Serialize, Deserialize)]
pub struct Results {
    pub status: String,
    pub token: Option<String>,
}

#[derive(Serialize)]
pub struct Scores {
    pub user_id: i32,
    pub username: String,
    pub score: i32,
}

#[derive(Serialize)]
pub struct PickedTeam {
    pub name: String,
    pub score: i32,
    pub confidence: i32,
    pub wins: i32,
}

#[derive(Serialize)]
pub struct HydratedPicks {
    pub user_id: i32,
    pub username: String,
    pub total_score: i32,
    pub picks: Vec<PickedTeam>,
}


async fn picks_by_user(conn: PgDb, req_user_id: i32) -> HydratedPicks {
    use schema::picks::dsl::*;
    use schema::teams::dsl::*;
    use schema::users::dsl::*;
    let (db_teams, db_picks, db_user) = conn.run(move |c| {
        let t = teams.load::<models::Team>(c).expect("Error loading teams");
        let p = picks.filter(user_id.eq(req_user_id)).load::<models::Pick>(c).expect("Error loading picks");
        let u: models::User = users.find(req_user_id).first(c).expect("could not find user");
        (t, p, u)
    }).await;
    let mut hydrated_picks = HydratedPicks {
        user_id: req_user_id,
        username: db_user.username,
        total_score: 0,
        picks: Vec::new(),
    };
    for pick in db_picks {
        if let Some(db_team) = db_teams.iter().find(|tt| tt.id == pick.team_id) {
            let score = db_team.wins * pick.confidence;
            hydrated_picks.picks.push(PickedTeam{
                name: db_team.team.clone(),
                wins: db_team.wins,
                confidence: pick.confidence,
                score,
            });
            hydrated_picks.total_score = hydrated_picks.total_score + score;
        }
    }
    hydrated_picks.picks = hydrated_picks.picks.into_iter().sorted_by(|a, b| Ord::cmp(&b.confidence, &a.confidence)).collect_vec();
    hydrated_picks
}

#[get("/sensitive")]
pub async fn sensitive(token: auth::TokenBody, conn: PgDb) -> Json<Vec<models::Team>> {
    use schema::teams::dsl::*;
    println!("{:?}", token);
    conn.run(|c| Json(teams.load::<models::Team>(c).expect("Error loading teams"))).await
}

#[post("/login", data="<user>")]
pub async fn login(conn: PgDb, user: Json<User<'_>>) -> Json<Results> {
    use schema::users::dsl::*;
    let uname = String::from(user.username);
    let req_pass = String::from(user.password);
    let user: Result<Vec<models::User>, diesel::result::Error> = conn.run(|c| {
        users.filter(username.eq(uname)).load::<models::User>(c)
    }).await;
    let user = match user {
        Ok(user) => user,
        Err(err) => panic!("{}", err),
    };
    if user.len() == 0 {
        return Json(Results {
            status: "Invalid username or password".to_string(),
            token: None 
        });
    }
    let user = &user[0];
    let result = match verify(req_pass, &user.password) {
        Ok(valid) if valid => {
            let token = auth::create_token(auth::TokenBody {
                id: user.id,
                username: user.username.to_string()
            });
            Results { status: "Success".to_string(), token: Some(token) }
        },
        _ => Results { status: "Invalid username or password".to_string(), token: None }
    };
    Json(result)
}

#[post("/register", data="<user>")]
pub async fn register(conn: PgDb, user: Json<User<'_>>, _before: time_guards::BeforeKickoff) -> Json<Results> {
    use schema::users::dsl::*;
    use schema::teams::dsl::*;
    use schema::picks::dsl::*;
    use schema::users::id as user_id_type;
    let uname = String::from(user.username);
    let pass = String::from(user.password);
    if let Ok(hashed) = hash(pass, DEFAULT_COST) {
        let user_name = uname.clone();
        let new_user = conn.run(|c| {
            let t = teams.load::<models::Team>(c).expect("could not get teams");
            let new_user = diesel::insert_into(users).values((username.eq(uname), password.eq(hashed))).returning(user_id_type).get_results::<i32>(c).expect("could not insert user");
            let new_user = new_user[0];
            let new_picks = t.iter().enumerate().map(|(index, db_team)| (user_id.eq(new_user), team_id.eq(db_team.id), confidence.eq(index as i32 + 1))).collect_vec();
            diesel::insert_into(picks).values(&new_picks).execute(c).expect("could not insert picks");
            new_user
        }).await;
        let token = auth::create_token(auth::TokenBody {
            id: new_user,
            username: user_name,
        });
        Json(Results { status: "Success".to_string(), token: Some(token) })
    } else {
        println!("could not hash the password");
        Json(Results { status: "Failed to register".to_string(), token: None })
    }
}

#[get("/teams", format="application/json")]
pub async fn teams(conn: PgDb) -> Json<Vec<models::Team>> {
    use schema::teams::dsl::*;
    conn.run(|c| Json(teams.load::<models::Team>(c).expect("Error loading teams"))).await
}

#[get("/picks", format="application/json")]
pub async fn picks(conn: PgDb, token: auth::TokenBody) -> Json<HydratedPicks> {
    Json(picks_by_user(conn, token.id).await)
}

#[get("/picks/<user_id>", format="application/json")]
pub async fn picks_for_user(conn: PgDb, _token: auth::TokenBody, user_id: i32, _after: time_guards::AfterKickoff) -> Json<HydratedPicks> {
    Json(picks_by_user(conn, user_id).await)
}

#[post("/picks", data="<selections>", format="application/json")]
pub async fn submit_picks(conn: PgDb, token: auth::TokenBody, _before: time_guards::BeforeKickoff, selections: Json<Vec<String>>) -> Json<Results> {
    use schema::picks::dsl::*;
    use schema::teams::dsl::*;
    let u_id = token.id;
    let db_teams: Vec<models::Team> = conn.run(move |c| {
        let t = teams.load::<models::Team>(c).expect("Error loading teams");
        diesel::delete(picks.filter(user_id.eq(u_id))).execute(c).expect("could not delete user picks");
        t
    }).await;
    let selections = selections.0;
    let selections = selections
        .into_iter()
        .map(move |s| db_teams.iter().find(|t| t.team == s).expect("could not find team").id);
    let new_picks = selections
        .into_iter()
        .enumerate()
        .map(|(index, selection)| (
            user_id.eq(u_id),
            team_id.eq(selection),
            confidence.eq(14 - index as i32)
        )).collect_vec();

    conn.run(move |c| {
        diesel::insert_into(picks).values(&new_picks).execute(c).expect("could not insert picks");
    }).await;
    Json(Results {
        status: "success".to_string(),
        token: None,
    })
}

#[get("/scores", format="application/json")]
pub async fn scores(conn: PgDb, _token: auth::TokenBody) -> Json<Vec<Scores>> {
    use schema::picks::dsl::*;
    use schema::teams::dsl::*;
    use schema::users::dsl::*;
    let (db_teams, db_users, db_picks) = conn.run(|c| {
        let t = teams.load::<models::Team>(c).expect("could not get teams");
        let u = users.load::<models::User>(c).expect("could not get users");
        let p = picks.load::<models::Pick>(c).expect("could not get picks");
        (t, u, p)
    }).await;

    let mut scores: Vec<Scores> = vec![];
    for (user, user_picks) in &db_picks.into_iter().group_by(|pick| pick.user_id) {
        let user = db_users.iter().find(|u| u.id == user).expect("could not find team id");
        let mut score = Scores { 
            user_id: user.id,
            username: user.username.clone(),
            score: 0 
        };

        for pick in user_picks {
            let db_team = db_teams.iter().find(|t| t.id == pick.team_id).expect("could not get teams");
            score.score = score.score + db_team.wins * pick.confidence;
        }
        scores.push(score);
    }
    let scores = scores
        .into_iter()
        .sorted_by(|a, b| Ord::cmp(&b.score, &a.score))
        .collect_vec();

    Json(scores)
}