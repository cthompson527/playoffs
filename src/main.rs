#[macro_use] extern crate rocket;
#[macro_use] extern crate diesel;
#[macro_use] extern crate lazy_static;
extern crate bcrypt;

mod schema;
mod models;
mod handlers;
mod connection;
mod auth;
mod time_guards;

use dotenv;

#[launch]
fn rocket() -> _ {
    dotenv::dotenv().ok();
    rocket::build()
        .attach(connection::PgDb::fairing())
        .mount("/", routes![
            handlers::teams,
            handlers::picks,
            handlers::picks_for_user,
            handlers::submit_picks,
            handlers::login,
            handlers::sensitive,
            handlers::scores,
            handlers::register,
        ])
}


#[cfg(test)]
mod test {
    use super::rocket;
    use rocket::local::blocking::Client;
    use rocket::http::Status;
    use crate::handlers::Results;

    #[test]
    fn test_login_returns_a_token() {
        let client = Client::tracked(rocket()).expect("valid rocket instance");
        let response = client.post("/login")
            .body("{\"username\": \"Cory\", \"password\": \"1234\"}")
            .dispatch();
        assert_eq!(response.status(), Status::Ok);
        if let Some(result) = response.into_json::<Results>() {
            assert_eq!(result.status, "Success");
            assert!(result.token.is_some())
        } else {
            panic!("could not parse into response into Results")
        }
    }
}
