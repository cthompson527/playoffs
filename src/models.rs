use rocket::serde::{Serialize, Deserialize};
use crate::schema::{users, picks, teams};

#[derive(Identifiable, Queryable, Serialize, Deserialize, Debug)]
pub struct User {
    pub id: i32,
    pub username: String,
    pub password: String,
}

#[derive(Identifiable, Queryable, Serialize, Deserialize, Debug)]
pub struct Team {
    pub id: i32,
    pub team: String,
    pub wins: i32,
}

#[derive(Identifiable, Queryable, Serialize, Deserialize, Associations, Debug)]
#[belongs_to(User)]
#[belongs_to(Team)]
pub struct Pick {
    pub id: i32,
    pub user_id: i32,
    pub team_id: i32,
    pub confidence: i32,
}

