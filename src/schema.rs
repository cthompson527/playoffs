table! {
    picks (id) {
        id -> Int4,
        user_id -> Int4,
        team_id -> Int4,
        confidence -> Int4,
    }
}

table! {
    teams (id) {
        id -> Int4,
        team -> Varchar,
        wins -> Int4,
    }
}

table! {
    users (id) {
        id -> Int4,
        username -> Varchar,
        password -> Varchar,
    }
}

joinable!(picks -> teams (team_id));
joinable!(picks -> users (user_id));

allow_tables_to_appear_in_same_query!(
    picks,
    teams,
    users,
);
