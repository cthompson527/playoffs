use rocket::{request::{FromRequest, Outcome}, Request};
use rocket::http::Status;
use chrono::prelude::*;

lazy_static! {
  static ref KICKOFF: DateTime<Utc> = Utc.ymd(2022, 1, 15).and_hms(21, 30, 0);
}

#[derive(Debug)]
pub enum TimeError {
    TooEarly,
    TooLate,
}

pub struct BeforeKickoff {}
#[rocket::async_trait]
impl<'r> FromRequest<'r> for BeforeKickoff {
  type Error = TimeError;
  
  async fn from_request(_req: &'r Request<'_>) -> Outcome<Self, Self::Error> {
    let now = Utc::now();
    if now > *KICKOFF {
      return Outcome::Failure((Status::BadRequest, TimeError::TooLate));
    }
    Outcome::Success(BeforeKickoff{})
  }
}

pub struct AfterKickoff {}
#[rocket::async_trait]
impl<'r> FromRequest<'r> for AfterKickoff {
  type Error = TimeError;
  
  async fn from_request(_req: &'r Request<'_>) -> Outcome<Self, Self::Error> {
    let now = Utc::now();
    if now < *KICKOFF {
      return Outcome::Failure((Status::BadRequest, TimeError::TooEarly));
    }
    Outcome::Success(AfterKickoff{})
  }
}
