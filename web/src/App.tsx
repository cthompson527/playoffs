import { AppBar, Box, Menu, MenuItem, Stack, Toolbar } from "@mui/material";
import React from "react";
import { NavLink, Outlet } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import isBefore from "date-fns/isBefore";
import kickoffTime from "./utils/kickoffTime";


const App = () => {
  const navigate = useNavigate();

  const logout = () => {
    localStorage.clear();
    navigate("/login");
  };

  return (
    <>
      <AppBar position="static">
        <Toolbar>
          <Stack spacing={2} direction="row">
            <MenuItem component={NavLink} to="/">
              Home
            </MenuItem>
            {isBefore(new Date(), kickoffTime()) && (
              <MenuItem component={NavLink} to="selections">
                Selections
              </MenuItem>
            )}
          </Stack>
          <Box sx={{ marginLeft: 'auto' }}>
            <Stack>
              <MenuItem onClick={logout}>Logout</MenuItem>
            </Stack>
          </Box>
        </Toolbar>
      </AppBar>
      <Box sx={{ maxWidth: '95vw', justifyContent: 'center' }}>
        <Outlet />
      </Box>
    </>
)
};

export default App;
