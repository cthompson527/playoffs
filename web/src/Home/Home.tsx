import { Typography } from "@mui/material";
import React from "react";
import Scores from "./Scores";

const Home = () => (
  <>
    <Typography variant="h1">
      Home
    </Typography>
    <Scores />
  </>
);

export default Home;
