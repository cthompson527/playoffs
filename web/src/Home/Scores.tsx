import { Table, TableCell, TableRow, TableHead, TableBody } from "@mui/material";
import React from "react";
import { useNavigate } from 'react-router-dom';
import memoizedApi from "../utils/fetch";
import isAfter from "date-fns/isAfter";
import kickoffTime from "../utils/kickoffTime";

const { query } = memoizedApi;

interface Score {
  user_id: number;
  username: string;
  score: number;
}

const Scores = () => {
  const [scores, setScores] = React.useState<Score[]>([]);
  const navigate = useNavigate();

  React.useEffect(() => {
    (async () => {
      const newScores = await query('/api/scores');
      setScores(newScores);
    })();
  }, []);
  
  return (
    <Table>
      <TableHead>
        <TableRow>
          <TableCell>Rank</TableCell>
          <TableCell>User</TableCell>
          <TableCell>Score</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {scores.map((user, index) => (
          <TableRow key={user.username} onClick={() => isAfter(new Date(), kickoffTime()) && navigate(`/selections/${user.user_id}`)}>
            <TableCell>{index + 1}</TableCell>
            <TableCell>{user.username}</TableCell>
            <TableCell>{user.score}</TableCell>
          </TableRow>
        ))}
      </TableBody>
    </Table>
  );

}

export default Scores;
