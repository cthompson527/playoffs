import {
  Alert,
  Button,
  Card,
  CardActions,
  CardContent,
  Switch,
  TextField,
  Typography,
  Box,
  LinearProgress
} from "@mui/material";
import React from "react";
import useLogin from "./useLogin.hook";
import { useNavigate } from "react-router-dom";

const Login = () => {
  const [username, password, confirmPassword, onChange, isRegistration, flipRegistration] = useLogin();
  const [isError, setError] = React.useState(false);
  const [justRegistered, setJustRegistered] = React.useState(false);
  const [submitting, setSubmitting] = React.useState(false);
  const navigate = useNavigate();

  const handleSubmit = async () => {
    if (submitting) return;
    setSubmitting(true);
    const route = isRegistration ? '/api/register' : '/api/login';
    const resp = await fetch(route, {
      method: 'POST',
      body: JSON.stringify({
        username,
        password,
      }),
    });
    const { status, token } = await resp.json();
    if (status === 'Success') {
      localStorage.setItem('token', token);
      navigate("/");
    } else {
      setSubmitting(false);
      setError(true);
    }
  }

  const isConfirmError = () => {
    return password !== confirmPassword;
  }

  const isDisabled = () => {
    if (!isRegistration) return false;
    return !submitting && isConfirmError();
  }

  return (
    <Box sx={{ display: 'flex', justifyContent: 'center', marginTop: '5rem' }}>
      <Card>
        <CardContent>
          <Typography variant="h1">Login</Typography>
          <TextField
              error={isError}
              fullWidth
              id="username"
              label="Username"
              variant="standard"
              value={username}
              onChange={(e) => onChange("username", e.target.value)}
              onKeyDown={(e) => e.key === 'Enter' ? handleSubmit() : {}}
          />
          <TextField
              error={isError}
              fullWidth
              id="password"
              label="Password"
              variant="standard"
              type="password"
              value={password}
              onChange={(e) => onChange("password", e.target.value)}
              onKeyDown={(e) => e.key === 'Enter' ? handleSubmit() : {}}
          />
          {isRegistration && (
              <TextField
                  error={isConfirmError()}
                  helperText="passwords must match"
                  fullWidth
                  id="confirmPassword"
                  label="Confirm Password"
                  variant="standard"
                  type="password"
                  value={confirmPassword}
                  onChange={(e) => onChange("confirmPassword", e.target.value)}
                  onKeyDown={(e) => e.key === 'Enter' ? handleSubmit() : {}}
              />
          )}
        </CardContent>
        <CardActions>
          {submitting ? (
            <Box sx={{ width: '100%' }}>
              <LinearProgress />
            </Box>
          ) : (
            <>
              <Switch checked={isRegistration} onChange={() => flipRegistration()}/> Register
              <Button variant="contained" disabled={isDisabled()} style={{ marginLeft: "auto" }} onClick={() => handleSubmit()}>{ isRegistration ? "Register" : "Login" }</Button>
            </>
          )}
        </CardActions>
      </Card>
      {justRegistered && (
        <Alert variant="filled" severity="success" onClose={() => setJustRegistered(false)}>
          Registration successful. Please login.
        </Alert>
      )}
    </Box>
  );
};

export default Login;
