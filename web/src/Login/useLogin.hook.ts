import React from "react";

type onChangeType = (id: string, value: string) => void;
type flipRegistrationType = () => void;
function useLogin(): [string, string, string, onChangeType, boolean, flipRegistrationType] {
  const [username, setUsername] = React.useState("");
  const [password, setPassword] = React.useState("");
  const [confirmPassword, setConfirmPassword] = React.useState("");
  const [isRegistration, setRegistration] = React.useState(false);

  function onChange(id: string, value: string) {
    switch (id) {
      case "username": setUsername(value); break;
      case "password": setPassword(value); break;
      case "confirmPassword": setConfirmPassword(value); break;
      default: console.error("invalid id");
    }
  }

  function flipRegistration() {
    setRegistration(!isRegistration);
  }

  return [username, password, confirmPassword, onChange, isRegistration, flipRegistration];
}

export default useLogin;
