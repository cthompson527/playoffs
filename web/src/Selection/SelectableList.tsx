import React from 'react';
import {Table, TableBody, TableHead, TableRow, TableCell, Alert} from "@mui/material";
import useSelections from './useSelections.hook';
import { DragDropContext, DropResult, Droppable, Draggable } from 'react-beautiful-dnd';
import memoizedApi from '../utils/fetch';
import { useParams } from 'react-router-dom';
import isAfter from 'date-fns/isAfter';
import kickoffTime from '../utils/kickoffTime';
import DragHandleIcon from '@mui/icons-material/DragHandle';

const { update, query } = memoizedApi;

interface Team {
  name: string;
  score: number;
  confidence: number;
  wins: number;
}

interface PicksResponse {
  user_id: number;
  username: string;
  total_score: number;
  picks: Team[];
}

const getItemStyle = (isDragging: boolean) => ({
  background: isDragging ? 'lightgray' : 'white',
  minWidth: '100%',
});

const SelectableList = () => {
  const [teams, setTeams, reorderTeams] = useSelections<Team>([]);
  const [username, setUsername] = React.useState("");
  const [totalScore, setTotalScore] = React.useState(0);
  const [error, setError] = React.useState(false);
  const urlParams = useParams();
  const afterKickoff = isAfter(new Date(), kickoffTime());

  function onDragEnd(result: DropResult) {
    // dropped outside the list
    if (!result.destination) {
      return;
    }

    const newTeams = reorderTeams(result.source.index, result.destination.index);
    (async () => {
      const teamsList = newTeams.map(nt => nt.name);
      const resp = await update<{ status: string }>('/api/picks', JSON.stringify(teamsList), '/api/picks');
      if (resp.status !== "success") {
        setError(true);
      }
    })();
  }

  function isDisabled() {
    if (urlParams.userId) return true;
    return afterKickoff;
  }

  React.useEffect(() => {
    (async () => {
      const route = urlParams.userId ? `/api/picks/${urlParams.userId}` : '/api/picks';
      const picks = await query<PicksResponse>(route);
      setUsername(picks.username);
      setTotalScore(picks.total_score);
      setTeams(picks.picks);
    })();
  }, []);

  return (
    <>
      <Table sx={{ width: '100%' }}>
        <TableHead>
          <TableRow>
            <TableCell>Confidence</TableCell>
            <TableCell>Team</TableCell>
            {afterKickoff && <TableCell>Wins</TableCell>}
            {afterKickoff && <TableCell>Score</TableCell>}
          </TableRow>
        </TableHead>
        <DragDropContext onDragEnd={onDragEnd}>
          <Droppable droppableId="droppable">
            {provided => (
              <TableBody ref={provided.innerRef} {...provided.droppableProps}>
                {teams.map((team, index) => (
                  <Draggable draggableId={team.name} index={index} key={team.name} isDragDisabled={isDisabled()}>
                    {(provided, snapshot) => (
                      <TableRow
                        sx={getItemStyle(snapshot.isDragging)}
                        style={{userSelect: 'none'}}
                        ref={provided.innerRef}
                        onClick={(e) => e.preventDefault()}
                        onClickCapture={(e) => e.preventDefault()}
                        onTouchStart={(e) => e.preventDefault()}
                        onTouchStartCapture={(e) => e.preventDefault()}
                        onContextMenu={(e) => e.preventDefault()}
                        onContextMenuCapture={(e) => e.preventDefault()}
                        {...provided.draggableProps}
                        {...provided.dragHandleProps}
                      >
                        {!snapshot.isDragging && <TableCell>{14-index}</TableCell>}
                        <TableCell>{team.name}{!isDisabled() && !snapshot.isDragging && <DragHandleIcon style={{float: 'right', color: 'gray'}} />}</TableCell>
                        {afterKickoff && <TableCell>{team.wins}</TableCell>}
                        {afterKickoff && <TableCell>{team.score}</TableCell>}
                      </TableRow>
                    )}
                  </Draggable>
                ))}
                {afterKickoff && (
                  <TableRow>
                    <TableCell colSpan={3}>Total Score for {username}:</TableCell>
                    <TableCell>{totalScore}</TableCell>
                  </TableRow>
                )}
              </TableBody>
            )}
          </Droppable>
        </DragDropContext>
      </Table>
      {error && (
        <Alert variant="filled" severity="error" onClose={() => setError(false)}>
          Could not save
        </Alert>
      )}
    </>
  );
};

export default SelectableList;


