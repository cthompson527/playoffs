import { Typography } from "@mui/material";
import React from "react";
import SelectableList from "./SelectableList";

const SelectionPage = () => {
  return (
    <>
      <Typography variant="h1">
        Selections
      </Typography>
      <SelectableList />
    </>
  );
}

export default SelectionPage;
