import React from "react";

type DispatchNewTeams<T> = (newTeams: T[]) => void;
type DispatchReorderTeams<T> = (sourceIndex: number, destIndex: number) => T[];

function useSelections<T>(initialTeams: T[]): [T[], DispatchNewTeams<T>, DispatchReorderTeams<T>] {
  const [teams, setTeams] = React.useState<T[]>(initialTeams);
  
  function reorderTeams(source: number, destination: number): T[] {
    const result = Array.from(teams);
    const [removed] = result.splice(source, 1);
    result.splice(destination, 0, removed);
    setTeams(result);
    return result;
  }

  function setNewTeams(newTeams: T[]) {
    setTeams(newTeams);
  }

  return [teams, setNewTeams, reorderTeams];
}

export default useSelections;
