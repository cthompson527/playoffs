import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import CssBaseline from '@mui/material/CssBaseline';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import SelectionPage from './Selection/SelectionPage';
import { createTheme } from '@mui/material';
import { ThemeProvider } from '@emotion/react';
import Home from './Home/Home';
import Login from './Login/Login';

const A404 = () => <h1>Page not found</h1>


const grid = 8;
const theme = createTheme({
  typography: {
    // In Chinese and Japanese the characters are usually larger,
    // so a smaller fontsize may be appropriate.
    fontSize: 12,
    h1: {
      fontSize: '2.5rem',
      padding: `${grid * 2}px 0 0 ${grid * 4}px`,
      color: 'gray',
    },
  },
});

ReactDOM.render(
  <React.StrictMode>
    <CssBaseline>
      <ThemeProvider theme={theme}>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<App />}>
              <Route path="/" element={<Home />} />
              <Route path="selections/:userId" element={<SelectionPage />} />
              <Route path="selections" element={<SelectionPage />} />
            </Route>
            <Route path="/login" element={<Login />} />
            <Route path="*" element={<A404 />} />
          </Routes>
        </BrowserRouter>
      </ThemeProvider>
    </CssBaseline>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
