function getHeaders() {
  const headers = new Headers();
  headers.append("Content-Type", "application/json");
  const token = localStorage.getItem('token');
  if (token) {
    headers.append("Authorization", `Bearer ${localStorage.getItem("token")}`);
  }
  return headers;
}

function memoizeApi() {
  const c: {[r: string]: {}} = {};
  async function query<T = any>(route: string) {
    if (c[route]) {
      return c[route] as T;
    }

    const resp = await apiFetch<T>(route);
    c[route] = resp;
    return resp;
  }

  function clear(route: string) {
    if (c[route]) {
      delete c[route];
    }
  }

  async function update<T = any>(route: string, body: any, requery?: string) {
    const resp = await apiPost<T>(route, body);
    if (requery) {
      if (c[requery]) {
        delete c[requery];
      }
      query<T>(requery);
    }
    return resp;
  }

  return { query, clear, update };
}


async function apiFetch<T = any>(route: string) {
  const headers = getHeaders();
  const resp = await fetch(route, { headers });
  if (resp.status >= 400) {
    window.location.replace('/login');
  }
  return (await resp.json() as T);
}

async function apiPost<T = any>(route: string, body: any) {
  const headers = getHeaders();
  const method = 'POST';
  const resp = await fetch(route, { headers, body, method });
  return (await resp.json() as T);
}

export default memoizeApi();
